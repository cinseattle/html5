// set up variables
var gameloopId;
var speed = 4;
var horizontalSpeed = speed;
var verticalSpeed = -speed;
var screenWidth;
var screenHeight;
var gameRunning = false;
var showBounds = false;
var ctx;
var frameCount = 0;
var frameCountId;
var score = 0;
var level = 0;
var divLeftOffset = 0;
var divTopOffset = 0;

// create images
var paddleImg = new Image();
var backgroundImg = new Image();
var paddleBall = new Image();
var prizeIcon1 = new Image();
var prizeIcon2 = new Image();
var scoreImg = new Image();
var multiPoint = 0;
var currentBonusImage = 0;
var delayCount=0;
//var showBonus = false;
var bonusStarted;
var bonusActive = false;

// lives and bonus objects
var livesImages = new Array();
var bonusImages = new Array();

//set background sound, loop and reduce volume
var backgroundLoop = document.getElementById('backgroundLoop');

// base game object
function gameObject()
{
	this.x = 0;
	this.y = 0;
	this.image = null;
}

// extend object for paddle
function paddle() {};
paddle.prototype = new gameObject();
paddle.prototype.boing = false;
paddle.prototype.startBoing = null;
paddle.prototype.lastBoing = null;
paddle.prototype.wobble = false;

var paddle = new paddle();

// extend game object for ball
function ball() {};
ball.prototype = new gameObject();
ball.prototype.angle = 0;

var ball = new ball();

// extend game object for prize
var prizes = new Array();	
function Prize() {};
Prize.prototype = new gameObject();
Prize.prototype.row = 0;
Prize.prototype.col = 0;
Prize.prototype.angle = 0;
Prize.spinning = false;
Prize.hit = false;

// wait for dom and init game
$(window).ready(function() {
	init();
});

function init() {
	loadImages();
	initSettings();
	addEventHandlers();
	startGame();
	startFPSCounter();
}

function startGame() {
	score = 0;
	lives = 3;
	level = level + 1;
	speed = speed + level;
	startLevel(level);
}

function startLevel(level) {
	initPrizes();
}

function startFPSCounter()
{
	var start = new Date().getTime(),
		time = 0;
	function instance()
	{
		time += 1000;
		fps();

		var diff = (new Date().getTime() - start) - time;
		window.setTimeout(instance, (1000 - diff));
	}
	window.setTimeout(instance, 1000);
}

function addEventHandlers() {
	$('#container').mousemove(function(e) {
		paddle.x = e.pageX - (paddle.image.width/2) - divLeftOffset;
	});

	$('#BtnImgStart').click(function() {
		toggleGameplay();
	});

	$('#showHideBounds').click(function() {
		showBounds = !showBounds;
		gameLoop();
	});
}

function initSettings() {
	ctx = document.getElementById('canvas').getContext('2d');

	screenWidth = parseInt($('#canvas').attr('width'));
	screenHeight = parseInt($('#canvas').attr('height'));

	paddle.x = parseInt(screenWidth / 2);
	paddle.y = screenHeight - 75;

	ball.x = parseInt(screenWidth / 2);
	ball.y = parseInt(screenHeight / 2);

	var position = $("#container").offset();
	divLeftOffset = parseInt(position.left);
	divTopOffset = parseInt(position.top);

    $('#BtnImgStart').css( { 
    	position: 'absolute',
        left: divLeftOffset + 260 + 'px', 
        top: divTopOffset + 180 + 'px',
    });

    $('#BtnImgStart').show();
}

function loadImages()
{
	paddleImg.src = 'images/paddle.png';
	backgroundImg.src = 'images/background.jpg';
	paddleBall.src = 'images/ball.png';
	prizeIcon1.src = 'images/apple.png';
	prizeIcon2.src = 'images/cherry.png';
	scoreImg.src = 'images/score.png';

	paddle.image = paddleImg;
	ball.image = paddleBall;

	backgroundImg.onload = function() {
		gameLoop();
	};

}

function initPrizes() {
	var count = 0;
	for(var x = 0; x < 2; x++) {
		for(var y = 0; y < 12; y++) {
			prize = new Prize();
			if(x == 0) {
				prize.image = prizeIcon1;
			}
			if(x == 1) {
				prize.image = prizeIcon2;
			}

			prize.row = x;
			prize.col = y;
			prize.x = 48 * prize.col + 38;
			prize.y = 52 * prize.row + 38;
			prize.hit = false;
			prizes[count] = prize;
			count++;
		}
	}
}

function drawPrizes() {
	for(var x = 0; x < prizes.length; x++) {
		currentPrize = prizes[x];
		if(!currentPrize.hit) {
			if(currentPrize.spinning) {
				//alert("spinning" + currentPrize.angle);
				currentPrize.angle += 10;
				ctx.save();
					
				//Translate to the center of the prize (i.e. the point about which we are going to perform the rotation
				ctx.translate(currentPrize.x + (currentPrize.image.width/2), currentPrize.y + (currentPrize.image.height/2));
					
				//Perform the rotation based on the current prize angle
				ctx.rotate(currentPrize.angle * Math.PI/180);
					
				ctx.drawImage(currentPrize.image, - (currentPrize.image.width/2), - (currentPrize.image.width/2));
					
				showObjectBounds(currentPrize, - (currentPrize.image.width/2), - (currentPrize.image.width/2));
					
				ctx.restore();
					
				if(currentPrize.angle == 360) {
					currentPrize.hit = true;
					multiPoint++;
						
					//Update score based on row
					if(multiPoint > 3) {
						//we've hit more that 3 prizes in a row, so add bonus and show bonus sign
						score += (3-currentPrize.row) * (multiPoint);
						
						//showBonus = true;
						//showBonus();
						//bonusStarted = new Date().getTime();
					} else {
						score += ( 3 - currentPrize.row);
					}
						
					//Check if all prizes have been caught!
					if(allPrizesHit()) {
						newLevel.play();
						gameOver();
							
					}
				}
			} else {
				ctx.drawImage(currentPrize.image, prizes[x].x, prizes[x].y);
				showObjectBounds(currentPrize);
					
			}
		}
	}
}

function allPrizesHit() {
	for(var c = 0; c < prizes.length; c++) {
		checkPrize = prizes[c];
		if(checkPrize.hit == false) {
			return false;
		}
	}
	return true;
}

function drawPaddle() {
	showObjectBounds(paddle);
	if(!paddle.boing) {
		ctx.drawImage(paddle.image, paddle.x, paddle.y);
	} else {
		var sinceLastBoing = (new Date().getTime() - paddle.lastBoing);
		var sinceStartBoing = (new Date().getTime() - paddle.startBoing);
		
		if (sinceLastBoing > 40) {
			paddle.wobble = !paddle.wobble;
			paddle.lastBoing = new Date().getTime();
		}

		if (sinceStartBoing > 500) {
			paddle.boing = false;
			paddle.wobble = false;
		}

		if (paddle.wobble) {
			ctx.drawImage(paddle.image, paddle.x, paddle.y);
		} else {
			ctx.drawImage(paddle.image, paddle.x, paddle.y);
		}
	}
}

function drawBall() {
	ctx.save();

	ctx.translate(ball.x + (ball.image.width / 2), ball.y + (ball.image.height / 2));

	ball.angle += horizontalSpeed;
	if (ball.angle < 0) {
		ball.angle = 360;
	} else if (ball.angle > 360) {
		ball.angle = 0;
	}

	ctx.rotate(ball.angle * Math.PI / 180);

	showObjectBounds(ball, -(ball.image.width /2), -(ball.image.width /2));

	ctx.drawImage(ball.image, -(ball.image.width / 2), -(ball.image.width / 2));

	ctx.restore();
}

function drawScore() {
	ctx.drawImage(scoreImg, screenWidth-(scoreImg.width), 0);
	ctx.font = '12pt Arial';
	ctx.fillText('' + score, 610, 25);
}

function drawLives() {
	ctx.drawImage(livesImages[lives], 0, 0);
}

function hasBallHitEdge() {
	// did ball hit right hand side?
	if (ball.x > screenWidth - ball.image.width) {
		if(horizontalSpeed > 0) {
			horizontalSpeed = - horizontalSpeed;
		}
	}

	// did ball hit left hand side?
	if (ball.x < -10) {
		if (horizontalSpeed < 0) {
			horizontalSpeed = -horizontalSpeed;
		}
	}

	// did ball hit the bottom?
	if (ball.y > screenHeight - ball.image.height) {
		verticalSpeed = -speed;
		multiPoint = 0;

		toggleGameplay();

		//Wait 2 seconds and reset for next life
		setTimeout(function(){
			if(lives != 0) {
				//Place the animal on top of the mushroom
				ball.x = parseInt(screenWidth/2);
				ball.y = parseInt(screenHeight/2);
				$("#BtnImgStart").show();
				gameLoop();
			}
				
		}, 1000);

		lives -= 1;

		if (lives > 0) {
			drawLives();
		} else {
			gameOver();
		}
	}

	// did ball hit the top of the screen?
	if (ball.y < 0) {
		verticalSpeed = speed;
	}
}

function hasBallHitPaddle() {
	if (checkIntersect(ball, paddle, 5)) {
		if((ball.x + ball.image.width / 2) < (paddle.x + paddle.image.width * 0.25)) {
			horizontalSpeed = -speed;
		} 
		else if ((ball.x + ball.image.width / 2) < (paddle.x + paddle.image.width * 0.5)) {
			horizontalSpeed = -speed / 2;
		} 
		else if((ball.x + ball.image.width / 2) < (paddle.x + paddle.image.width * 0.75)) {
			horizontalSpeed = speed / 2;
		}
		else {
			horizontalSpeed = speed;
		}
		paddle.startBoing = new Date().getTime();
		paddle.lastBoing = paddle.lastBoing;
		paddle.boing = true;
		verticalSpeed = -speed;
		multiPoint =0;
	}
}

function hasBallHitPrize() {
	for(var x = 0; x < prizes.length; x++) {
		var prize = prizes[x];

		if(!prize.hit) {
			if(checkIntersect(prize, ball, 0)) {
				prize.spinning = true;
				verticalSpeed = speed;
			}
		}
	}
}

function gameLoop() {
	backgroundLoop.play;

	ctx.clearRect(0,0,screenWidth,screenHeight);

	ball.x += horizontalSpeed;
	ball.y += verticalSpeed;

	ctx.drawImage(backgroundImg, 0, 0);	

	// draw score
	drawScore();

	// draw lives
	//drawLives();

	// draw prizes
	drawPrizes();
	
	drawPaddle();

	drawBall();

	hasBallHitEdge();

	hasBallHitPaddle();

	hasBallHitPrize();

	frameCount++;
}

function checkIntersect(object1, object2, overlap)
{
	//    x-Axis                      x-Axis
	//  A1------>B1 C1              A2------>B2 C2
	//  +--------+   ^              +--------+   ^ 
	//  | object1|   | y-Axis       | object2|   | y-Axis
	//  |        |   |              |        |   |
	//  +--------+  D1              +--------+  D2
	//
		
	A1 = object1.x + overlap;
	B1 = object1.x + object1.image.width - overlap;
	C1 = object1.y + overlap; 
	D1 = object1.y + object1.image.height - overlap;
		
	A2 = object2.x + overlap;
	B2 = object2.x + object2.image.width - overlap;
	C2 = object2.y + overlap;
	D2 = object2.y + object2.image.width - overlap;
		
	//Do they overlap on the x-Axis
	if(A1 > A2 && A1 < B2 || B1 > A2 && B1 < B2)
	{
		//x axis intersects so check y axis
		if(C1 > C2 && C1 < D2 || D1 > C2 && D1 < D2)
		{
			//overlap
			return true;
		}
			
	}
	//as you were
	return false;
}
	
	
function showObjectBounds(gameObject, transitionX, transitionY)
{
	if(showBounds)
	{
		if(typeof(transitionX) != 'undefined')
			rectX = transitionX;
		else
			rectX = gameObject.x;
		
		if(typeof(transitionY) != 'undefined')
			rectY = transitionY;
		else
			rectY = gameObject.y;
				
		ctx.save();
			
		ctx.strokeStyle = '#f00'; // red
		ctx.lineWidth   = 2;
		ctx.strokeRect(rectX, rectY, gameObject.image.width, gameObject.image.height);
			
		ctx.restore();
	}
}

//Update the display to show frames per second and reset ready for next count
function fps()
{
	$("#fps").html(frameCount + " fps");
	frameCount=0;
}	

//Start game timer, i.e. setTimeout that calls itself taking into account the
//actual real difference in time. This is better than 
function startGameTimer()
{
	var start = new Date().getTime(),
		time = 0;
	function timer()
	{
		time += 15;
		var diff = (new Date().getTime() - start) - time;
		if(gameRunning)
		{
			gameLoop();
			window.setTimeout(timer, (15 - diff));
		}
	}
	if(gameRunning)
		window.setTimeout(timer, 15);
}
	
// start/stop the game loop
function toggleGameplay()
{
	gameRunning = !gameRunning;
	
	if(gameRunning)
	{
		$("#BtnImgStart").hide();
		startGameTimer();
	}
}

// game over
function gameOver() {
	gameRunning = false;
	alert('Game Over: thanks for playing!');
}